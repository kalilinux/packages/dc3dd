#!/usr/bin/python
""" Copyright (C) 2009 Christophe Monniez
	This script is a helper for the Debian maintainer of this package.
	It search for the copyright informations from FSF inside ../lib/*.
"""

import os, os.path
import re

def getYears(ligne):
	yearsp = re.compile('([12]\d{3})|([12]\d{3}-[12]\d{3}),{0,1}')
	ylist = yearsp.findall(ligne)
	years = ''
	for y in ylist:
		 years += ', '.join(y)
	if years != '':
		return years
	else:
		return None

def getcopyright(fichier):
	f = open(fichier,'ro')
	content = f.read()
	cpp = re.compile(' Copyright \(C\).*\n{0,1}.*Free.*',re.MULTILINE)
	s = cpp.search(content)
	if s:
		crstring = cpp.search(content).group().replace('\n',' ').replace('\t','')
		years = getYears(crstring)
		return 'Copyright (C) ' + years + 'Free Software Foundation, Inc.'
	else:
		return "No copyright found"

crdic = {}
for racine,dirs,fichiers in os.walk('../lib/'):
	for fic in fichiers:
		cs = getcopyright(os.path.join(racine,fic))
		if crdic.has_key(cs):
			crdic[cs].append(fic)
		else:
			crdic[cs]=[]
			crdic[cs].append(fic)

for c in crdic.keys():
	fichiers = ', '.join(crdic[c])
	if len(crdic[c]) > 1:
		print "Files: %s" % fichiers
	else:
		print "File: %s" % fichiers
	print "Copyright: %s\n" % c
